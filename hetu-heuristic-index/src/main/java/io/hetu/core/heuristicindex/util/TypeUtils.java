/*
 * Copyright (C) 2018-2020. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.hetu.core.heuristicindex.util;

import io.airlift.slice.Slice;
import io.prestosql.spi.function.Signature;
import io.prestosql.spi.relation.CallExpression;
import io.prestosql.spi.relation.ConstantExpression;
import io.prestosql.spi.relation.RowExpression;
import io.prestosql.spi.type.BigintType;
import io.prestosql.spi.type.BooleanType;
import io.prestosql.spi.type.CharType;
import io.prestosql.spi.type.DecimalType;
import io.prestosql.spi.type.DoubleType;
import io.prestosql.spi.type.IntegerType;
import io.prestosql.spi.type.RealType;
import io.prestosql.spi.type.SmallintType;
import io.prestosql.spi.type.TimestampType;
import io.prestosql.spi.type.TinyintType;
import io.prestosql.spi.type.Type;
import io.prestosql.spi.type.VarcharType;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Locale;

import static com.google.common.base.Preconditions.checkState;
import static io.prestosql.spi.type.Decimals.decodeUnscaledValue;
import static java.lang.Float.intBitsToFloat;

public class TypeUtils
{
    private TypeUtils() {}

    private static final String CAST_OPERATOR = "$operator$cast";

    public static Object extractSingleValue(RowExpression rowExpression)
    {
        if (rowExpression instanceof CallExpression) {
            CallExpression callExpression = (CallExpression) rowExpression;
            Signature signature = callExpression.getSignature();
            String name = signature.getName().toLowerCase(Locale.ENGLISH);

            if (name.equals(CAST_OPERATOR)) {
                return extractSingleValue(callExpression.getArguments().get(0));
            }
        }
        else if (rowExpression instanceof ConstantExpression) {
            ConstantExpression constant = (ConstantExpression) rowExpression;
            Type type = constant.getType();

            if (type instanceof BigintType || type instanceof TinyintType || type instanceof SmallintType || type instanceof IntegerType) {
                return constant.getValue();
            }
            else if (type instanceof BooleanType) {
                return constant.getValue();
            }
            else if (type instanceof DoubleType) {
                return constant.getValue();
            }
            else if (type instanceof RealType) {
                Long number = (Long) constant.getValue();
                return intBitsToFloat(number.intValue());
            }
            else if (type instanceof VarcharType || type instanceof CharType) {
                if (constant.getValue() instanceof Slice) {
                    return ((Slice) constant.getValue()).toStringUtf8();
                }
                return constant.getValue();
            }
            else if (type instanceof DecimalType) {
                DecimalType decimalType = (DecimalType) type;
                if (decimalType.isShort()) {
                    checkState(constant.getValue() instanceof Long);
                    return new BigDecimal(BigInteger.valueOf((Long) constant.getValue()), decimalType.getScale(), new MathContext(decimalType.getPrecision()));
                }
                checkState(constant.getValue() instanceof Slice);
                Slice value = (Slice) constant.getValue();
                return new BigDecimal(decodeUnscaledValue(value), decimalType.getScale(), new MathContext(decimalType.getPrecision()));
            }
            else if (type instanceof TimestampType) {
                Long time = (Long) constant.getValue();
                return new Timestamp(time);
            }
        }

        throw new UnsupportedOperationException("Not Implemented Exception: " + rowExpression.toString());
    }

    public static Object getNativeValue(Object object)
    {
        return object instanceof Slice ? ((Slice) object).toStringUtf8() : object;
    }

    public static String extractType(Object object)
    {
        if (object instanceof Long) {
            return "Long";
        }
        else if (object instanceof String) {
            return "String";
        }
        else if (object instanceof Integer) {
            return "Integer";
        }
        else if (object instanceof Slice) {
            return "String";
        }
        else {
            throw new UnsupportedOperationException("Not a valid type to create index: " + object.getClass());
        }
    }

    public static Comparator<kotlin.Pair<? extends Comparable<?>, ?>> getComparator(String type)
    {
        switch (type) {
            case "long":
            case "Long":
            case "Slice":
            case "String":
            case "int":
            case "Integer":
                return (o1, o2) -> ((Comparable) o1.getFirst()).compareTo(o2.getFirst());
        }

        throw new RuntimeException("Type is not supported");
    }

    /**
     * Double is 8 bytes in java. This returns the 8 bytes of the decimal value of BigInteger
     * @param value
     * @return
     */
    public static byte[] getBytes(BigDecimal value)
    {
        byte[] bytes = new byte[8];
        java.nio.ByteBuffer.wrap(bytes).putDouble(value.doubleValue());
        return bytes;
    }

    /**
     * Double is 8 bytes in java. This returns the 8 bytes of the decimal value of Double
     * @param value
     * @return
     */
    public static byte[] getBytes(Double value)
    {
        byte[] bytes = new byte[8];
        java.nio.ByteBuffer.wrap(bytes).putDouble(value.doubleValue());
        return bytes;
    }
}
